from __future__ import print_function
from qtpy import QtWidgets, QtCore, QtGui
from kabaret.app import resources
import json
import os
import time
import gazu
from libreflow.baseflow.kitsu import KitsuAPIWrapper as kitsu
from kabaret import flow

try:
    from kabaret.app.ui.gui.widgets.widget_view import DockedView
except ImportError:
    raise RuntimeError('Your kabaret version is too old, for the Custom View. Please update !')


class AssetList(QtWidgets.QTableWidget):

    headers = ["Type", "Name", "Exists", "Status", "Oid"]
    asset_oid = ""

    def __init__(self, parent, session):
        super(AssetList, self).__init__(parent)
        self.session = session

        
        self.setColumnCount(len(self.headers))
        for col, header in enumerate(self.headers):
            header_item = QtWidgets.QTableWidgetItem(header)
            self.setHorizontalHeaderItem(col, header_item)
        
        self.setColumnHidden(4, True) # Oids

        self.setSortingEnabled(True)
        self.sortByColumn(len(self.headers), QtCore.Qt.DescendingOrder)

        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self._on_popup_menu_request)

        self._popup_menu = QtWidgets.QMenu(self)

        self._filter = None
        self._jid_to_item = {}

    def set_item_filter(self, filter):
        self._filter = filter
        for row in range(self.rowCount()):
            should_show = any(filter in self.item(row, col).text().lower() for col in range(self.columnCount()))
            self.setRowHidden(row, not should_show)

    def asset_exists(self, asset_name):
        asset_list_oid = self.session.cmds.Flow.call('/EWILAN', 'get_asset_list_oid', [], {})
        return self.session.cmds.Flow.call(asset_list_oid, 'asset_exists', [asset_name], {})
    

    def get_assets(self):
        if kitsu.current_user_logged_in(self):
            # print("User logged in...")
            project = gazu.project.get_project_by_name("EWILAN")

            character_type = gazu.asset.get_asset_type_by_name("CHARACTERS")
            character_data = gazu.asset.all_assets_for_project_and_type(project, character_type)

            creature_type = gazu.asset.get_asset_type_by_name("CREATURES")
            creature_data = gazu.asset.all_assets_for_project_and_type(project, creature_type)

            props_type = gazu.asset.get_asset_type_by_name("PROPS")
            props_data = gazu.asset.all_assets_for_project_and_type(project, props_type)

            
        assets = []

        rig_task = gazu.task.get_task_type_by_name("RIG_2D_CHARS")

        for ch in character_data:
            chara_tasks = gazu.task.all_tasks_for_asset(ch)
            for task in chara_tasks:
                if task["task_type_name"] == "RIG_2D_CHARS":
                    task_status = task["task_status_name"]
                    task_status_color = gazu.task.get_task_status_by_name(task_status)["color"]
                    break
            # print(ch["name"], " RIG2D : ", task_status, "color : ", task_status_color)
            assets.append({"type" : "CHARACTERS", "name" : ch["name"], "status": task_status, "color" : task_status_color})
        
        for cr in creature_data:
            crea_tasks = gazu.task.all_tasks_for_asset(cr)
            for task in crea_tasks:
                if task["task_type_name"] == "RIG_2D_CHARS":
                    task_status = task["task_status_name"]
                    task_status_color = gazu.task.get_task_status_by_name(task_status)["color"]
                    break
            assets.append({"type" : "CREATURES", "name" : cr["name"], "status": task_status, "color" : task_status_color})
        
        for pr in props_data:
            props_tasks = gazu.task.all_tasks_for_asset(pr)
            for task in props_tasks:
                if task["task_type_name"] == "RIG_2D_CHARS":
                    task_status = task["task_status_name"]
                    task_status_color = gazu.task.get_task_status_by_name(task_status)["color"]
                    break
            assets.append({"type" : "PROPS", "name" : pr["name"], "status": task_status, "color" : task_status_color})
        return assets

    def add_assets(self):
        
        try:
            print("Adding Assets ! ....")

            assets = self.get_assets()
            self.setRowCount(len(assets))
            for row ,asset in enumerate(assets):
                
                item_type = QtWidgets.QTableWidgetItem(asset["type"])
                item_name = QtWidgets.QTableWidgetItem(asset["name"])
                item_exists = QtWidgets.QTableWidgetItem("")

                exists_check = self.asset_exists(asset["name"])
                self.asset_oid = exists_check[1]
                item_oid = QtWidgets.QTableWidgetItem(self.asset_oid)

                if exists_check[0]:
                    # print("Asset : ", self.asset_oid, " exists")
                    item_exists.setText("true")
                    item_exists.setForeground(QtGui.QColor(53, 219, 114))
                    item_exists.setBackground(QtGui.QColor(53, 219, 114))
                else:
                    item_exists.setText("false")
                    item_exists.setForeground(QtGui.QColor(219, 59, 53))
                    item_exists.setBackground(QtGui.QColor(219, 59, 53))

                # How to check if the asset is ready ?
                # Ask Kitsu :
                # If the asset is type Character ==> check if RIG_2D_CHARS is set to "OK"
                # If the asset is type Creature ==>
                # If the asset is type Props ==> check if RIG_2D_PROPS is set to "OK"
                item_is_ready = QtWidgets.QTableWidgetItem("")
                item_is_ready.setText(asset["status"])
                item_is_ready.setForeground(QtGui.QColor(asset["color"]))
                item_is_ready.setBackground(QtGui.QColor(asset["color"]))

                
                # Set items as not editable
                item_type.setFlags(item_type.flags() ^ QtCore.Qt.ItemIsEditable)
                item_name.setFlags(item_name.flags() ^ QtCore.Qt.ItemIsEditable)
                item_exists.setFlags(item_exists.flags() ^ QtCore.Qt.ItemIsEditable)
                item_is_ready.setFlags(item_is_ready.flags() ^ QtCore.Qt.ItemIsEditable)
                item_oid.setFlags(item_is_ready.flags() ^ QtCore.Qt.ItemIsEditable)
                
                # Insert items into the table
                self.setItem(row, 0, item_type)
                self.setItem(row, 1, item_name)
                self.setItem(row, 2, item_exists)
                self.setItem(row, 3, item_is_ready)
                self.setItem(row, 4, item_oid)

        except Exception as e:
            print("Error in add_assets:", e)
        
        self.resizeColumnsToContents()
        self.viewport().repaint()

    def _on_popup_menu_request(self, pos):
        # Here check the asset oid and propose a menu to go to the asset tasks page

        item = self.itemAt(pos)
        # print(item, "at pos : ", pos)
        # print("Row number : ", item.row())
        item_oid = self.item(item.row(), 4).text()
        # print("Corresponding oid : ", item_oid)
        m = self._popup_menu
        m.clear()
        oid = item_oid
        name = os.path.basename(oid)
        if oid:
            m.addAction('Open {} asset page'.format(name), lambda item=item: self._goto(item, item_oid))
            self._popup_menu.popup(self.viewport().mapToGlobal(pos))
            pass

    def _goto(self, item, item_oid):
        if not self.session.is_gui():
            return

        oid = item_oid
        wm = self.session
        view = wm.add_view('Flow', area=None, oid=oid)
        # ensure visible if tabbed:
        view.show()

class AssetView(DockedView):

    @classmethod
    def view_type_name(cls):
        return 'Asset View'

    def __init__(self, *args, **kwargs):
        super(AssetView, self).__init__(*args, **kwargs)

    def _build(self, top_parent, top_layout, main_parent, header_parent, header_layout):
        self.add_header_tool('*', '*', 'Duplicate View', self.duplicate_view)

        self._filter_le = QtWidgets.QLineEdit(main_parent)
        self._filter_le.setPlaceholderText('Filter Assets...')
        self._filter_le.textChanged.connect(self._on_filter_change)

        self._asset_list = AssetList(main_parent, self.session)

        lo = QtWidgets.QVBoxLayout()
        lo.setContentsMargins(0, 0, 0, 0)
        lo.setSpacing(0)
        lo.addWidget(self._filter_le)
        lo.addWidget(self._asset_list)

        main_parent.setLayout(lo)
        self.view_menu.setTitle('Assets')

        a = self.view_menu.addAction('Refresh', self.add_assets)

    def _on_filter_change(self):
        self._asset_list.set_item_filter(self._filter_le.text().lower())

    def add_assets(self):
        self._asset_list.add_assets()

    def on_show(self):
        # self._asset_list.add_assets()
        pass
        
    def receive_event(self, event, data):
        pass
