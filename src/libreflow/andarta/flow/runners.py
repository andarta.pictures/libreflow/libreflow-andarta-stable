import libreflow.baseflow.runners as runner
from ..resources import icons 


runner.FILE_EXTENSIONS.append("comp")
runner.FILE_EXTENSIONS.append("psd")
runner.FILE_EXTENSIONS.append("mp4")
runner.FILE_EXTENSION_ICONS.update({"comp": ("icons", "fusion"), "psd": ("icons.flow", "photoshop"), "mp4": ("icons", "djv")})

class Fusion(runner.EditFileRunner):

    ICON = ("icons", "fusion")
    TAGS = [
        "Compositing"
    ]

    @classmethod
    def supported_versions(cls):
        return ["1.0"]

    @classmethod
    def supported_extensions(cls):
        return [".comp",]

class Photoshop(runner.EditFileRunner):

    ICON = ("icons.flow", "photoshop")
    TAGS = [
        "Paint"
    ]

    @classmethod
    def supported_versions(cls):
        return ["2023", "2024"]

    @classmethod
    def supported_extensions(cls):
        return [".psd","psb"]
    
class Djv(runner.EditFileRunner):

    ICON = ("icons", "djv")
    TAGS = [
        "Player"
    ]

    @classmethod
    def supported_versions(cls):
        return ["1.0"]

    @classmethod
    def supported_extensions(cls):
        return [".mp4"]

